# **Food Safety Inspection** #
## **Field Assistant** ##

Development of a field assistant for an inspection company in the field of restoration establishments, applying the method Cascade in the developing of this software project. 
This service has two larger components each one composed of smaller components. These two components are:

* **Back Office component** is used for the creation of activities and communication plans with the inspected establishments.
* **Mobile component** is used in the field and should help the supervisor, guiding him and allowing him to save all that is necessary about their activities in the field.